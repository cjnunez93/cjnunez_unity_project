﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour {



	public GUIText TimerText;
	private float TimerStart; 

	void Start ()
	{
		TimerStart = 0.0f; 

	}



	void Update ()
	{
		TimerStart += Time.deltaTime;
		TimerText.text = "Timer: " + TimerStart; 
	}
		
}
