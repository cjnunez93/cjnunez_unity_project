﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}
public class PlayerController : MonoBehaviour 
{
    public float speed;
    public float tilt;
    public Boundary boundary;

    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate;

	public GameObject shot2;
	public Transform shotSpawn2;
	public float fireRate2;

	public GameObject shot3;
	public Transform shotSpawn3;
	public float fireRate3;

	public GameObject shot4;
	public Transform shotSpawn4;
	public float fireRate4;




    private float nextFire;

	private float nextFire2;

	private float nextFire3;

	private float nextFire4;

	Rigidbody rb;


    void Update ()
	{
		if (Input.GetButton ("Fire1") && Time.time > nextFire) {
			nextFire = Time.time + fireRate;
			// newProjectile = 
			Instantiate (shot, shotSpawn.position, shotSpawn.rotation); // as GameObject;

			if (Input.GetButton ("Fire1") && Time.time > nextFire2) {
				nextFire2 = Time.time + fireRate2;
				// newProjectile = 
				Instantiate (shot2, shotSpawn2.position, shotSpawn2.rotation); // as GameObject;

				if (Input.GetButton ("Fire1") && Time.time > nextFire3) {
					nextFire3 = Time.time + fireRate3;
					// newProjectile = 
					Instantiate (shot3, shotSpawn3.position, shotSpawn3.rotation); // as GameObject;

					if (Input.GetButton ("Fire1") && Time.time > nextFire4) {
						nextFire4 = Time.time + fireRate4;
						// newProjectile = 
						Instantiate (shot4, shotSpawn4.position, shotSpawn4.rotation); // as GameObject;


					}
				}
			}
		}

	}
// 2nd shot code 
//	void Update2 ()
//	{


//	}
    

     void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rb.velocity = movement * speed;

        rb.position = new Vector3
            (
                Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax), 
                0.0f,
                Mathf.Clamp(rb.position.z, boundary.zMin, boundary.zMax)

            );
        rb.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x * -tilt);

	}
}
